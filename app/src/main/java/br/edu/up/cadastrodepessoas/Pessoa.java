package br.edu.up.cadastrodepessoas;

import java.io.Serializable;

public class Pessoa implements Serializable {

    private int id;
    private String nome;
    private String sobrenome;

    @Override
    public String toString() {
        return nome + " " + sobrenome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
}
