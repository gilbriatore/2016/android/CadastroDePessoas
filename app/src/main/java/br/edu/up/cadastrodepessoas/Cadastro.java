package br.edu.up.cadastrodepessoas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class Cadastro extends SQLiteOpenHelper {


    public Cadastro(Context context) {
        super(context, "banco", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE `pessoa` (" +
                "`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "`nome` TEXT, `sobrenome` TEXT );";

        db.execSQL(sql);

    }

    public void gravar(Pessoa pessoa){

        ContentValues valores = new ContentValues();
        valores.put("nome", pessoa.getNome());
        valores.put("sobrenome", pessoa.getSobrenome());

        SQLiteDatabase db = getWritableDatabase();

        if (pessoa.getId() > 0){
            //Atualizar
            int id = pessoa.getId();
            String _id = String.valueOf(id);
            db.update("pessoa", valores, "id = ?", new String[]{_id});
        } else {
            //Incluir
            db.insert("pessoa", null, valores);
        }
        db.close();
    }

    public void excluir(Pessoa pessoa){
        SQLiteDatabase db = getWritableDatabase();
        int id = pessoa.getId();
        String _id = String.valueOf(id);
        db.delete("pessoa", "id = ?", new String[]{_id});
        db.close();
    }

    public Pessoa buscar(int id){
        SQLiteDatabase db = getReadableDatabase();
        String _id = String.valueOf(id);
        Cursor cursor = db.query("pessoa",null,"id = ?",
                new String[]{_id},null, null, null);

        Pessoa pessoa = null;

        if (cursor.moveToNext()){
            int index1 = cursor.getColumnIndex("id");
            int index2 = cursor.getColumnIndex("nome");
            int index3 = cursor.getColumnIndex("sobrenome");
            pessoa = new Pessoa();
            pessoa.setId(cursor.getInt(index1));
            pessoa.setNome(cursor.getString(index2));
            pessoa.setSobrenome(cursor.getString(index3));
        }
        db.close();
        return pessoa;
    }


    public List<Pessoa> listar(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("pessoa",null,null,
                null,null, null, null);

        List<Pessoa> pessoas = new ArrayList<>();

        while (cursor.moveToNext()){
            int index1 = cursor.getColumnIndex("id");
            int index2 = cursor.getColumnIndex("nome");
            int index3 = cursor.getColumnIndex("sobrenome");
            Pessoa pessoa = new Pessoa();
            pessoa.setId(cursor.getInt(index1));
            pessoa.setNome(cursor.getString(index2));
            pessoa.setSobrenome(cursor.getString(index3));

            pessoas.add(pessoa);
        }
        db.close();
        return pessoas;
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Não implementado ainda.
    }
}
