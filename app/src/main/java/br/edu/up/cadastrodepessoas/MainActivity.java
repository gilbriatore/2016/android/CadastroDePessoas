package br.edu.up.cadastrodepessoas;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    Cadastro cadastro;
    ArrayAdapter<Pessoa> adapter;
    List<Pessoa> pessoas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cadastro = new Cadastro(this);
        pessoas = cadastro.listar();
        int layout = android.R.layout.simple_list_item_1;
        adapter = new ArrayAdapter<>(this, layout, pessoas);

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa pessoa = adapter.getItem(position);
                Intent intent = new Intent(getBaseContext(), CadastroActivity.class);
                intent.putExtra("id", pessoa.getId());
                startActivityForResult(intent, 0);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Pessoa pessoa = adapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Atenção");
                builder.setMessage("Deseja excluir o(a) " + pessoa.getNome() + "?");
                builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cadastro.excluir(pessoa);
                        pessoas.clear();
                        pessoas.addAll(cadastro.listar());
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      //ignorada
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });

    }

    public void onClickCadastrar(View v){

        Intent intent = new Intent(this, CadastroActivity.class);
        //startActivity(intent);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        pessoas.clear();
        pessoas.addAll(cadastro.listar());
        adapter.notifyDataSetChanged();

    }
}
