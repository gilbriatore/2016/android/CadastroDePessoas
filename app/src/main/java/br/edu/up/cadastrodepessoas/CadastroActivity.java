package br.edu.up.cadastrodepessoas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CadastroActivity extends AppCompatActivity {


    Pessoa pessoa;
    EditText txtNome;
    EditText txtSobrenome;
    Cadastro cadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        txtNome = (EditText) findViewById(R.id.txtNome);
        txtSobrenome = (EditText) findViewById(R.id.txtSobrenome);

        cadastro = new Cadastro(this);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);
        if (id > 0){
            pessoa = cadastro.buscar(id);
            txtNome.setText(pessoa.getNome());
            txtSobrenome.setText(pessoa.getSobrenome());
        }
    }

    public void onClickGravar(View v){

        String nome = txtNome.getText().toString();
        String sobrenome = txtSobrenome.getText().toString();

        if (pessoa == null){
            pessoa = new Pessoa();
        }

        pessoa.setNome(nome);
        pessoa.setSobrenome(sobrenome);
        cadastro.gravar(pessoa);


        //Encerra a activity
        finish();
    }

}
